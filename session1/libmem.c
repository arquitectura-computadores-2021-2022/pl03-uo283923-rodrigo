/*! \file libmem.c
    \brief Functions to walk the page table of the current process.

	Functions to walk the page table of the current process using /dev/linmem device.

	Version 1.0, July 2016
	Authors: Juan Carlos Granda and Jose Maria Lopez
	       University of Oviedo
*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "include/linmem.h"
#include "include/memdata.h"


//! \def DEVICE
/*!
	File for the linmem device.
*/
#define DEVICE "/dev/linmem"


//! Returns the page table entry (PTE) for a virtual address.
/*!
	It establishes a dialog with the linmem device as follows:
	1.- Open the device file.
	2.- Send the virtual address
	3.- Read the content of the PTE.
*/
#if __x86_64__
	int get_pte(void *virtual_addr, unsigned long *p_pte)
#else
	int get_pte(void *virtual_addr, unsigned int *p_pte)
#endif
{
	int file;
	struct mem_data data;

	file = open(DEVICE, O_RDWR);
	if (file < 0) {
		return -1;
    }

	if (write(file, &virtual_addr, sizeof(void *))!= 0) {
		return -1;
    }

	if (read(file, &data, sizeof(struct mem_data)) != 0) {
		return -1;
    }

	close(file);

	if (p_pte != NULL) {
		*p_pte = data.pte;
	}
    return 0;
}
