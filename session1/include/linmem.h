/*! \file linmem.h
    \brief Functions to walk the page table of the current process.

	Functions to walk the page table of the current process using /dev/linmem device.

	Version 1.0, July 2016
	Authors: Juan Carlos Granda and Jose Maria Lopez
	       University of Oviedo
*/
#pragma once

#if __x86_64__

	//! Returns the page table entry (PTE) for a 64-bit virtual address.
	/*!
		\param virtual_addr the virtual address.
		\param p_pte output variable where the PTE content is written or 0 if the virtual address does not have PTE.
		\return 0 when successful and -1 when error or there is no PTE. Check the errno variable.
	*/
	int get_pte(void *virtual_addr, unsigned long *p_pte);

#else

	//! Returns the page table entry (PTE) for a 32-bit virtual address.
	/*!
		\param virtual_addr the virtual address.
		\param p_pte output variable where the PTE content is written or 0 if the virtual address does not have PTE.
		\return 0 when successful and -1 when error or there is no PTE. Check the errno variable.
	*/
	int get_pte(void *virtual_addr, unsigned int *p_pte);

#endif
