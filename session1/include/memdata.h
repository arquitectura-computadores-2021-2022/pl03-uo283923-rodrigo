/*! \file memdata.h
    \brief Tipos para el intercambio de datos entre el driver linmem y su biblioteca de usuario.

	Define tipos de datos para el intercambio de informacion entre el driver del dispositivo /dev/linmem y su biblioteca de usuario.

	Versi�n 1.0, Julio de 2016
	Autor: Juan Carlos Granda y Jose Maria Lopez
	       Universidad de Oviedo
*/
#pragma once


//! Datos que se intercambian driver y biblioteca.
/*! Datos que envia el driver tras pasarle una direccion virtual. */
struct __attribute__((__packed__)) mem_data {
	unsigned long content;    /*!< Contenido (64 bits) de la direccion virtual. */
	void *p_pgd;              /*!< Direccion virtual de la entrada en el primer nivel de la tabla de paginas asociada a la direccion virtual. */
	unsigned long pgd;        /*!< Contenido de la entrada en el primer nivel de la tabla de paginas asociada a la direccion virtual. */
	void *p_pud;              /*!< Direccion virtual de la entrada en el segundo nivel de la tabla de paginas asociada a la direccion virtual. */
	unsigned long pud;        /*!< Contenido de la entrada en el segundo nivel de la tabla de paginas asociada a la direccion virtual. */
	void *p_pmd;              /*!< Direccion virtual de la entrada en el tercer nivel de la tabla de paginas asociada a la direccion virtual. */
	unsigned long pmd;        /*!< Contenido de la entrada en el tercer nivel de la tabla de paginas asociada a la direccion virtual. */
	void *p_pte;              /*!< Direccion virtual de la entrada en el cuarto nivel de la tabla de paginas asociada a la direccion virtual. */
	unsigned long pte;        /*!< Contenido de la entrada en el cuarto nivel de la tabla de paginas asociada a la direccion virtual. */
};
