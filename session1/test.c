#include <stdio.h>
#include <atc/linmem.h>

int main(void)
{
  #if __x86_64__
    unsigned long pte = 0;
    printf("64 bits\n");
  #else
    unsigned int pte = 0;
    printf("32 bits\n");
  #endif

  if (get_pte(main, &pte) != 0)
  {
    printf("---- FAILURE ----\n");
    perror("Error");
  }
  else
  {
    printf("---- SUCCESS ----\n");
    printf("Virtual address: %ph\n", main);
    #if __x86_64__
      printf("PTE: %lXh\n", pte);
    #else
      printf("PTE: %Xh\n", pte);
    #endif
  }

  return 0;
}
