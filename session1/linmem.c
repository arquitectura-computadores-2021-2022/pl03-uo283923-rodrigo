/*! \file linmem.c
    \brief Kernel module for the /dev/linmem device.

	The linmem device is a character device that can be used to walk the page table of a process.

	Version 1.0, July 2016
	Authors: Juan Carlos Granda and Jose Maria Lopez
	       University of Oviedo
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/sched.h>
#include <linux/mm.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/highmem.h>
#include <asm/uaccess.h>
#include <asm/current.h>
#include <asm/pgtable.h>
#include "include/memdata.h"


MODULE_LICENSE("GPL");
MODULE_AUTHOR("ATC (UniOvi)");
MODULE_DESCRIPTION("Walk the page table of a process");

//! \def DEVICE
/*!
	Name of the linmem device in the /dev directory.
*/
#define DEVICE "linmem"

int            init_module(void);
void           cleanup_module(void);
static int     device_open(struct inode *, struct file *);
static int     device_release(struct inode *, struct file *);
static ssize_t device_read(struct file *, char __user *,
			   size_t, loff_t *);
static ssize_t device_write(struct file *, const char __user *,
                            size_t, loff_t *);


/*! \var int major
    \brief Mayor number of the device.
*/
static int major;
/*! \var void *virtual_addr
    \brief Virtual address.
*/
static void * virtual_addr = NULL;
/*! \var struct mutex mutex
    \brief Mutex to use the device.
*/
struct mutex mutex;


/*! \var struct file_operations fops
    \brief Operations allowed.
*/
static struct file_operations fops =
{
  .owner   = THIS_MODULE,
  .read    = device_read,
  .write   = device_write,
  .open    = device_open,
  .release = device_release
};



//! Called when the module is loaded.
/*!
*/
static int __init linmem_init(void)
{
	int new_major = 0;

	printk(KERN_ALERT "Installing device %s\n", DEVICE);

	// Mutual exclusion
	mutex_init(&mutex);

	new_major = register_chrdev(0, DEVICE, &fops);
	if (new_major < 0) {
		printk(KERN_ALERT "Error registering device %s\n", DEVICE);
		return new_major;
	}
	major = new_major;

	return 0;
}


//! Called when the module is unloaded.
/*!
*/
static void __exit linmem_exit(void)
{
	printk(KERN_ALERT "Removing device %s\n", DEVICE);
	unregister_chrdev(major, DEVICE);
}


//! Called when opening the device.
/*!
*/
static int device_open(struct inode *p_inode, struct file *p_file)
{
	mutex_lock_interruptible(&mutex);
	// Increment the reference count for the device
	try_module_get(THIS_MODULE);
	return 0;
}


//! Called when closing the device.
/*!
*/
static int device_release(struct inode *p_inode, struct file *p_file)
{
  module_put(THIS_MODULE);  /* Decrement the reference count for the device */
  mutex_unlock(&mutex);
  return 0;
}



//! Called when the device is written.
/*!
	Called when the device is written (and open). The device waits for a virtual address.
*/
static ssize_t device_write(struct file *p_file, const char __user *p_buffer, 
                            size_t count, loff_t *p_offset)
{
	if (count < sizeof(void *)) {
		printk(KERN_ALERT "Error writing to device %s\n", DEVICE);
		return -1;
	}

	// Reads the user buffer for a virtual address
	return copy_from_user(&virtual_addr, p_buffer, sizeof(void *));
}


//! Called when the device is read.
/*!
	Called when the device is read (and open). The device copies to the user buffer:
   - Virtual address where PGD is located.
   - Content of the PGD.
   - Virtual address where PUD is located.
   - Content of the PUD.
   - Virtual address where PMD is located.
   - Content of the PMD.
   - Virtual address where PTE is located.
   - Content of the PTE.
   - Content of the virtual address.

   The functions checks that the "length" parameter is 9 times the word of the processor.
   It return 0 for no error. */
static ssize_t device_read(struct file *p_file, char __user *p_buffer,
			   size_t count, loff_t *p_offset)
{
	struct mem_data data;

	if (count < sizeof(struct mem_data)) {
		printk(KERN_ALERT "Error reading from device %s\n", DEVICE);
		return -1;
	}

	memset(&data, 0, sizeof(struct mem_data));

	spin_lock(&current->mm->page_table_lock);

	// Walk through the levels of the page table
	data.p_pgd = pgd_offset(current->mm, (unsigned long)virtual_addr);
	data.pgd = pgd_val(*(pgd_t*)data.p_pgd);

	if (!pgd_none(*(pgd_t*)data.p_pgd) && !pgd_bad(*(pgd_t*)data.p_pgd)) {

		data.p_pud = pud_offset(data.p_pgd, (unsigned long)virtual_addr);
		data.pud = pud_val(*(pud_t*)data.p_pud);

		if (!pud_none(*(pud_t*)data.p_pud) && !unlikely(pud_bad(*(pud_t*)data.p_pud))) {

			data.p_pmd = pmd_offset(data.p_pud, (unsigned long)virtual_addr);
			data.pmd = pmd_val(*(pmd_t*)data.p_pmd);

			if (!pmd_none(*(pmd_t*)data.p_pmd) && !unlikely(pmd_bad(*(pmd_t*)data.p_pmd))) {

				data.p_pte = pte_offset_map((pmd_t*)data.p_pmd, (unsigned long)virtual_addr);
				if (data.p_pte) {
					data.pte = pte_val(*(pte_t*)data.p_pte);

					if (pte_present(*(pte_t*)data.p_pte)) {
						data.content = *((unsigned long *)virtual_addr);
					}
				}
				pte_unmap(data.p_pte);
			}
		}
	}

	spin_unlock(&current->mm->page_table_lock);
	return copy_to_user(p_buffer, &data, sizeof(struct mem_data));
}


module_init(linmem_init);
module_exit(linmem_exit);
